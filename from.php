<?php
require_once("inc/init.php");

$oClient = new TequilaClient();
$oClient->SetApplicationName('AGEPoly Zoom Authentication');
$oClient->SetWantedAttributes(array('uniqueid', 'name', 'firstname', 'email'));
$oClient->SetApplicationURL(APP_URL."from.php");

$oClient->SetCustomFilter("statut=etudiant");

if(isset($_GET["ag_guest"]))
{
  $rows = $dbRegistered->query("SELECT COUNT(*) as count FROM registered WHERE zoom_email == '".SQLite3::escapeString($_GET["ag_guest"])."' OR email == '".SQLite3::escapeString($_GET["ag_guest"])."'");
  $rows2 = $dbRegistered->query("SELECT COUNT(*) as count FROM tobreakout WHERE email == '".SQLite3::escapeString($_GET["ag_guest"])."'");
  if($rows->fetchArray()["count"]>0 && $rows2->fetchArray()["count"]<1) {exit("Account Of Student - Forbid");}
  $email = SQLite3::escapeString($_GET["ag_guest"]);
  $firstName = "Guest";
  $lastName  = "No Vote";
  
  $_SESSION["brk"] = true;
}
else if(isset($_GET["zoom_log_test"]))
{
  $email = $_GET["zoom_log_test"];
  $firstName = "zoom";
  $lastName  = "zoom";
}
else if (isset($_SESSION["email"]))
{
  $firstName = $_SESSION["firstname"];
  $lastName = $_SESSION["lastname"];
  $email = $_SESSION["email"];
}
else
{
  $oClient->Authenticate();
  $email = $oClient->getValue('email');
  $firstName = $oClient->getValue('firstname');
  $lastName  = $oClient->getValue('name');
}


// Storing session data
$token = bin2hex(random_bytes(32));

$_SESSION['token'] = $token;

$_SESSION["firstname"] = $firstName;
$_SESSION["lastname"] = $lastName;
$_SESSION["email"] = $email;
?>

<!DOCTYPE html>
<html>

<head>
  <?php include("inc/head.html"); ?>
</head>

<body>
  <div class="alert" onclick="this.style.display='none';"
    style="display: <?php if(isset($_GET["success"])){ echo 'block';} else if(isset($_GET["error"])){ echo 'block'; } else { echo 'none'; } ?>;">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <?php if(isset($_GET["success"])){ echo 'Réussite';} else if(isset($_GET["error"])){ echo 'Erreur'; } ?>
  </div>
  <div class="splash-container">
    <div class="splash">
      <h1 class="splash-head">Assemblée Générale de l'AGEPoly</h1>
      <?php
        $res = $dbRegistered->query("SELECT * FROM registered WHERE email = '$_SESSION[email]'");
        if($a = $res->fetchArray()){
        $response = new stdClass();
        $response->join_url = $a["url"];
        
        echo '<p class="splash-subhead" style="text-align: center;">Compte zoom enregistré : '.$a["zoom_email"].'</p>';
        
        echo '<a class="pure-button go-ag" href="'.$response->join_url.'">ALLER A L\'AG</a>';
        
        echo '<p class="splash-subhead more-infos">Votre lien personnel pour l\'AG : <p>';
        echo '<a style="word-break: break-all;" class="more-infos" href="'.$response->join_url.'">'.$response->join_url.'</a>';
      ?>
      <form class="pure-form" action="/back_mail.php" method="get">
        <legend>Changement de compte zoom</legend>
        <a class="pure-button pure-button-primary"
          href="https://zoom.us/oauth/signin?_rnd=1587070319161&client_id=kn53xjY3RTqmi_XI3eXUhg&redirect_uri=https%3A%2F%2Fag.agepoly.ch%2Fback.php&response_type=code&state=<?php echo $token ?>">Connexion
          Zoom</a>
      </form>
      <?php
        } else {
      ?>
      <div class="">
        <form class="pure-form">
          <legend>Inscription à l'AG - <?php echo $_SESSION["email"] ?></legend>
          <a class="pure-button pure-button-primary"
            href="https://zoom.us/oauth/signin?_rnd=1587070319161&client_id=kn53xjY3RTqmi_XI3eXUhg&redirect_uri=https%3A%2F%2Fag.agepoly.ch%2Fback.php&response_type=code&state=<?php echo $token ?>">Connexion
            Zoom</a>
        </form>
        <p class="splash-subhead">You need to login to zoom in order for us to know wich zoom account you use.
        </p>
      </div>
      <?php } ?>
    </div>
  </div>
</body>

</html>