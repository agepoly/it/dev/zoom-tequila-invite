<?php
require_once("inc/conf.php");
require_once("inc/functions.php");
require_once("tequila/tequila.php");
session_set_cookie_params(["SameSite" => "Strict"]);

session_start();

$dbRegistered = new SQLite3('/app/keys/registered.db');
