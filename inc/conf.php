<?php

//Mettre à true pour afficher des erreurs détaillées
define("DEBUG", false);

//Note : cette valeur doit aussi être changée dans les pararmères de l'application sur Zoom
define("APP_URL",       "https://ag.agepoly.ch/");
define("APP_FRONT_URL", "https://ag.agepoly.ch/back.php");


define("ZOOM_MEETING_ID", "62341384164");

define("ZOOM_TOKEN_FILE",         "keys/token.txt");
define("ZOOM_REFRESH_TOKEN_FILE", "keys/refreshToken.txt");

define("APP_CLIENT_ID",          "uiXQnI80TRGvMiCya5coA");
define("APP_CLIENT_SECRET",      "PH4Ph4WANeCIoXoq6jWbvayu6OG53dWH");
define("APP_AUTHORIZATION_CODE", "6davMyWlJo_cBUl7HKrRP2p6rMYMEQt7w");


define("APP_FRONT_CLIENT_ID",     "uiXQnI80TRGvMiCya5coA");
define("APP_FRONT_CLIENT_SECRET", "PH4Ph4WANeCIoXoq6jWbvayu6OG53dWH");


define("ADM_PASSWORD", "hcwbvacnjewirzwed7263uhr3r43545");

if(DEBUG){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ERROR);
} else {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
}

/*
    Fonction pour obtenir le token d'accès d'une app
*/
function generateToken(){
    $code = base64_encode(APP_CLIENT_ID.":".APP_CLIENT_SECRET);
    
    $post = [
        'grant_type'   => 'authorization_code',
        'code'         => APP_AUTHORIZATION_CODE,
        'redirect_uri' => APP_URL
    ];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,"https://zoom.us/oauth/token");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$code));
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function createDb(){
    $db = new SQLite3('/app/keys/registered.db');
    $db->exec("CREATE TABLE registered(id INTEGER PRIMARY KEY, email TEXT, zoom_email TEXT, url TEXT)");
    $db->exec("CREATE TABLE todel(id INTEGER PRIMARY KEY, email TEXT)");
    $db->exec("CREATE TABLE tobreakout(id INTEGER PRIMARY KEY, email TEXT)");

}
?>
