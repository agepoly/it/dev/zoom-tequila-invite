<?php
function registerUser($user){
	$curl = curl_init();
	$zoomToken = file_get_contents(ZOOM_TOKEN_FILE);
	
	curl_setopt($curl, CURLOPT_URL,        "https://api.zoom.us/v2/meetings/".ZOOM_MEETING_ID."/registrants");
	curl_setopt($curl, CURLOPT_POST,       1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		"authorization: Bearer ".$zoomToken,
		"content-type: application/json",
		"content-length: ".strlen(json_encode($user))
	));
	curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
	$response = json_decode(curl_exec($curl));
	$err      = curl_error($curl);
	curl_close($curl);
	
       debugMessageAndObj("register user", $response);
	
	if(isset($response->code, $response->message) && 
	$response->code == 124 && 
	$response->message == "Access token is expired."){
		return $response;
	}
	
        if(isset($response->code, $response->message) && 
        $response->code == 300 ){
            printError("error pushing person", $response);
            exit();
        }

	if ($err) {
		printError("Erreur lors de l'enregistrement de l'utilisateur, la connexion a échoué", $err);
	}
	
	$curl2 = curl_init(); 
	
	$datas = new stdClass(); 
	$uc = new stdClass();
	$uc->email = $user->email;
	$datas->action      = "approve"; 
	$datas->registrants      = array($uc); 
	
	
	curl_setopt($curl2, CURLOPT_URL,        "https://api.zoom.us/v2/meetings/".ZOOM_MEETING_ID."/registrants/status"); 
	curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, "PUT"); 
	curl_setopt($curl2, CURLOPT_POSTFIELDS, json_encode($datas)); 
	curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($curl2, CURLOPT_HTTPHEADER, array( 
		"authorization: Bearer ".$zoomToken, 
		"content-type: application/json", 
		"content-length: ".strlen(json_encode($datas))
	)); 
	curl_setopt($curl2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
	$response2 = json_decode(curl_exec($curl2)); 
	$err2 = curl_error($curl2); 
	curl_close($curl2); 
	
	if(isset($response2->code, $response2->message) && 
	$response2->code == 124 && 
	$response2->message == "Access token is expired."){
		return $response2;
	}
	
	if ($err2) { 
		printError("Erreur lors de la validation de l'utilisateur, la connexion a échoué", $err2); 
    }
    
        debugMessageAndObj("approve user", $response2);
	
	$curl3 = curl_init(); 
	
	curl_setopt($curl3, CURLOPT_URL,        "https://api.zoom.us/v2/meetings/".ZOOM_MEETING_ID."/registrants/?status=approved&page_size=300"); 
	curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($curl3, CURLOPT_HTTPHEADER, array( 
		"authorization: Bearer ".$zoomToken, 
		"content-type: application/json" 
	)); 
	curl_setopt($curl3, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
	$response3 = json_decode(curl_exec($curl3)); 
	$err3 = curl_error($curl3); 
	curl_close($curl3); 
    
        debugMessageAndObj("get user join url", $response3);
	
	if ($err3) { 
		printError("Erreur lors de la requête de list de l'utilisateur, la connexion a échoué", $err3); 
	}
	
	if (!isset($response3->registrants))
	{
		return $response3;
	}
	
	foreach ($response3->registrants as $u) 
	{
		if ($u->email == $user->email )
		{ 
			return $u;
		}
	}
	printError("pas trouvé l'user", $response3); 
	
}

function refreshToken(){
	$curl = curl_init();
	$code = base64_encode(APP_CLIENT_ID.":".APP_CLIENT_SECRET);
	
	$post = [
		'grant_type'    => 'refresh_token',
		'refresh_token' => file_get_contents(ZOOM_REFRESH_TOKEN_FILE)
	];
	
	curl_setopt($curl, CURLOPT_URL,            "https://zoom.us/oauth/token");
	curl_setopt($curl, CURLOPT_POST,           1);
	curl_setopt($curl, CURLOPT_POSTFIELDS,     $post);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER,     array("Authorization: Basic ".$code));
	
	$response = json_decode(curl_exec($curl));
	$err      = curl_error($curl);
	curl_close($curl);

        debugMessageAndObj("refresh token", $response);
	
	if ($err) {
		printError("Erreur lors de l'actualisation du token, la connexion a échoué", $err);
		return false;
	} else {
		if(isset($response->refresh_token, $response->access_token )){
			file_put_contents(ZOOM_REFRESH_TOKEN_FILE, $response->refresh_token);
			file_put_contents(ZOOM_TOKEN_FILE,         $response->access_token);
			return true;
		}else{
			printError("Erreur lors de l'actualisation du token, le serveur a refusé la requête", $response);
			return false;
		}
	}
}

function printAccessMessage($response){
    debugMessage("redirect to from");
    header('Location: '.APP_URL.'?success');
}

function deny($user){
	$curl4 = curl_init(); 
	$zoomToken = file_get_contents(ZOOM_TOKEN_FILE);
	
	$datas = new stdClass(); 
	$uc = new stdClass();
	$uc->email = $user->email;
	$datas->action      = "deny"; 
	$datas->registrants      = array($uc); 
	curl_setopt($curl4, CURLOPT_URL,        "https://api.zoom.us/v2/meetings/".ZOOM_MEETING_ID."/registrants/status"); 
	curl_setopt($curl4, CURLOPT_CUSTOMREQUEST, "PUT"); 
	curl_setopt($curl4, CURLOPT_POSTFIELDS, json_encode($datas)); 
	curl_setopt($curl4, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($curl4, CURLOPT_HTTPHEADER, array( 
		"authorization: Bearer ".$zoomToken, 
		"content-type: application/json" 
	)); 
	curl_setopt($curl4, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
	$response4 = json_decode(curl_exec($curl4)); 
	$err4 = curl_error($curl4); 
	curl_close($curl4); 
    
    debugMessageAndObj("deny user", $response4);
	
	if ($err4) { 
		printError("Erreur lors de du deny de l'utilisateur, la connexion a échoué", $err4); 
		exit();
	}
}

function requireSessionMailAndValidToken(){
    if(!isset($_SESSION["email"])){
        printError("Not connected",$_SESSION);
        exit();
    }
    
    if(!isset($_SESSION["token"]) || !isset($_GET["token"]) || !($_GET["token"] == $_SESSION["token"])){
        printError("CSRF error",$_SESSION);
        exit();
    }
}

function printError($errorMsg, $response){
	if(DEBUG){
		echo("  <p>Une erreur est survenue : ".$errorMsg."</p>
		        <p>Réponse : <pre>");print_r($response);echo("</pre></p>");
	}else{
		echo("  <p>Une erreur est survenue : ".$errorMsg."</p>
		        <p>Merci de contacter comite@agepoly.ch</p>");
	}
}

function debugMessage($msg){
    if(DEBUG){
        echo "<br>".$msg."<br>";
    }
}

function debugMessagePrintR($obj){
    if(DEBUG){
        print_r($obj);
    }
}

function debugMessageAndObj($msg, $obj){
    debugMessage($msg);
    debugMessagePrintR($obj);
}

function secureAdmin(){
	if(!$_GET['adm'] == ADM_PASSWORD) {    
		header('HTTP/1.0 401 Unauthorized');
		exit();
	}
}
