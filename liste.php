<?php
require_once("inc/init.php");

function listRegistrants(){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.zoom.us/v2/meetings/".ZOOM_MEETING_ID."/registrants?page_number=10&page_size=300&status=approved",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array("authorization: Bearer ".file_get_contents(ZOOM_TOKEN_FILE)),
        )
    );

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        return "cURL Error #:".$err;
    } else {
        return json_decode($response);
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <?php 
            $result = listRegistrants();
            if(!$result->registrants) { echo $result; return; }
            foreach($result->registrants as $curUser){
                echo "<p>".$curUser->first_name." ".$curUser->last_name."</p>";
                echo "<p>".$curUser->email."</p>";
                echo "<hr>";
            }
        ?>
    </body>
</html>