FROM php:7.4-apache

ADD vhost.conf /etc/apache2/sites-available/000-default.conf
ADD apache.conf /etc/apache2/conf-available/z-app.conf

ADD . /app

RUN apt-get update && apt-get install sqlite3 -y

RUN a2enconf z-app