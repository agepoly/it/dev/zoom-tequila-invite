<!DOCTYPE html>
<html>
<head>
  <?php include("inc/head.html"); ?>
</head>
<body>
  <div class="splash-container">
    <div class="splash">
      <h1 class="splash-head">Assemblée Générale de l'AGEPoly</h1>
      <form class="pure-form" action="/" method="get">
        <legend>Inscriptions invités AG</legend>
        <input name="ag_guest" type="text" placeholder="votre nom" required/> 
        <button type="submit" value="Submit" class="pure-button pure-button-primary">Inscription</button>
      </form>
    </div>
  </div>
</body>
</html>