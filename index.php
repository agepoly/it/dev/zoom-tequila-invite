<?php
require_once("inc/init.php");

$oClient = new TequilaClient();
$oClient->SetApplicationName('AGEPoly Zoom Authentication');
$oClient->SetWantedAttributes(array('uniqueid', 'name', 'firstname', 'email', 'unit'));
$oClient->SetApplicationURL(APP_URL);
$oClient->SetCustomFilter("statut=etudiant");

if(isset($_GET["ag_guest"]))
{
  $rows  = $dbRegistered->query("SELECT COUNT(*) as count FROM registered WHERE zoom_email == '".SQLite3::escapeString($_GET["ag_guest"])."' OR email == '".SQLite3::escapeString($_GET["ag_guest"])."'");
  $rows2 = $dbRegistered->query("SELECT COUNT(*) as count FROM tobreakout WHERE email == '".SQLite3::escapeString($_GET["ag_guest"])."'");

  if( $rows->fetchArray()["count"] > 0 && $rows2->fetchArray()["count"] < 1 ) {
    exit("Account Of Student - Forbid");
  }

  $_SESSION["email"]     = SQLite3::escapeString($_GET["ag_guest"]);
  $_SESSION["firstname"] = SQLite3::escapeString($_GET["ag_guest"]);
  $_SESSION["lastname"]  = "No Vote";
  
  $_SESSION["brk"] = true;
}
else if(isset($_GET["zoom_log_test"]))
{
  $_SESSION["firstname"] = "zoom";
  $_SESSION["lastname"]  = "zoom";
  $_SESSION["email"]     = $_GET["zoom_log_test"];
}
else if(isset($_SESSION["email"]))
{
  $_SESSION["firstname"] = $_SESSION["firstname"];
  $_SESSION["lastname"]  = $_SESSION["lastname"];
  $_SESSION["email"]     = $_SESSION["email"];
}
else
{
  $oClient->Authenticate();

  $_SESSION["firstname"] = $oClient->getValue('firstname');
  $_SESSION["lastname"]  = $oClient->getValue('name');
  $_SESSION["email"]     = $oClient->getValue('email');
}

// Storing session data
$_SESSION['token'] = bin2hex(random_bytes(32));

?>

<!DOCTYPE html>
<html>
<head>
  <?php include("inc/head.html"); ?>
</head>
<body>
  <?php
    if(isset($_GET["success"]) || isset($_GET["error"])){ 
        $class = "";
        if(isset($_GET["success"])){$class = "success";}
        if(isset($_GET["error"])){$class = "error";}
  ?>
      <div class="<?= $class ?> alert" onclick="this.style.display='none';">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <?php 
          if(isset($_GET["success"])){ 
            echo 'Réussite';
          }else if(isset($_GET["error"])){ 
            echo 'Erreur'; 
          } 
        ?>
      </div>
  <?php } ?>
  <div class="splash-container">
    <div class="splash">
      <h1 class="splash-head">Assemblée Générale de l'AGEPoly</h1>
        <?php
        $res = $dbRegistered->query("SELECT * FROM registered WHERE email = '$_SESSION[email]'");
        if($a = $res->fetchArray()){ ?>
        
          <p class="splash-subhead" style="text-align: center;">Compte zoom enregistré : <?=$a["zoom_email"]?></p>
          <a class="pure-button go-ag" href="<?=$a["url"]?>">ALLER A L'AG</a>
          <p class="splash-subhead more-infos">Votre lien personnel pour l'AG : <p>
          <a style="word-break: break-all;" class="more-infos" href="<?=$a["url"]?>"><?=$a["url"]?></a>

          <form class="pure-form" action="/back_mail.php" method="get">
            <legend>Changement de compte zoom</legend>
            <input name="email" type="email" placeholder="Email du compte zoom" required>
            <input name="token" type="hidden" value="<?= $_SESSION['token'] ?>">
            <button type="submit" value="Submit" class="pure-button pure-button-primary">Changer</button>
          </form>

        <?php 
        } else { 
        ?>

          <form class="pure-form" action="/back_mail.php" method="get">
            <legend>Inscription à l'AG - <?= $_SESSION["email"] ?></legend>
            <input name="email" type="email" placeholder="Email du compte zoom" required>
            <input name="token" type="hidden" value="<?= $_SESSION['token'] ?>">
            <button type="submit" value="Submit" class="pure-button pure-button-primary">S'enregistrer</button>
          </form>

        <?php } ?>
      <p class="splash-subhead">@AGEPoly - Corentin Junod, Téo Goddet</p>
    </div>
  </div>
</body>
</html>