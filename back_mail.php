<?php
require_once("inc/init.php");
requireSessionMailAndValidToken();

$zoomParticipantData = new stdClass();
$zoomParticipantData->email      = SQLite3::escapeString($_GET["email"]);
$zoomParticipantData->first_name = $_SESSION["firstname"];
$zoomParticipantData->last_name  = $_SESSION["lastname"];

$res1 = $dbRegistered->query("SELECT * FROM registered WHERE email != '".$_SESSION["email"]."'AND zoom_email = '".$zoomParticipantData->email."'");

if($res1->fetchArray()){
	header('Location: '.APP_URL.'?error=zoom_account_already_used');
	debugMessage("redirect to from");
	exit();
}

$res = $dbRegistered->query("SELECT * FROM registered WHERE email = '".$_SESSION["email"]."'");


if($a = $res->fetchArray()){
	debugMessageAndObj("user from db", $a);

	if($a["zoom_email"] == $zoomParticipantData->email){
		header('Location: '.APP_URL.'?success=zoom_account_unchanged');
		debugMessage("redirect to from");
		exit();
	} else { 
		//deny previous mail and let flow to the register part
		$response = deny($zoomParticipantData);
		
		if(isset($response->code, $response->message) && 
			$response->code == 124 && 
			$response->message == "Access token is expired."){
			
			if(refreshToken()){
				$response = deny($zoomParticipantData);
				if(!isset($response->code)){
					$r = $dbRegistered->exec("DELETE FROM registered WHERE email = '".$_SESSION["email"]."'");
					$dbRegistered->exec("INSERT INTO todel(email) VALUES('".$a["zoom_email"]."')");

					if(!$r){
						printError("db delete error", $r); 
						exit();
					}
				} else {
					printError("Erreur inconnue", $response);
					exit();
				}
			} else {
				printError("Erreur lors de l'actualisation du token", $response);
				exit();
			}
			
		} else if(isset($response->code)) {
			printError("Erreur inconnue", $response);
			exit();
			
		} else {
			$r = $dbRegistered->exec("DELETE FROM registered WHERE email = '".$_SESSION["email"]."'");
			$dbRegistered->exec("INSERT INTO todel(email) VALUES('".$a["zoom_email"]."')");
			if(!$r){
				printError("db delete error", $r); 
				exit();
			}
		}
	}
}
$response = registerUser($zoomParticipantData);

if(isset($response->code, $response->message) && 
                        $response->code == 124 && 
                        $response->message == "Access token is expired."){

  if(refreshToken()){
   $response = registerUser($zoomParticipantData);
  } else {
   printError("refresh token error", $response);
  }
}

if(isset($response->join_url)){
	$r = $dbRegistered->exec("INSERT INTO registered(email, zoom_email, url) VALUES('".$_SESSION["email"]."', '".$zoomParticipantData->email."', '".$response->join_url."')");
	$dbRegistered->exec("DELETE FROM todel WHERE email = '".$zoomParticipantData->email."'");
       
        if(!$r){
          printError("record in db error", $r); 
          exit();
        }

 	if(isset($_SESSION["brk"]) && $_SESSION["brk"]){
		$dbRegistered->exec("INSERT INTO tobreakout(email) VALUES('".$zoomParticipantData->email."')");
	}
	printAccessMessage($response);
}  else {
	printError("Erreur inconnue", $response);
}

printAccessMessage(null);
	
?>
