<?php
require_once("inc/init.php");
requireSessionMailAndValidToken();

$code = base64_encode(APP_FRONT_CLIENT_ID.":".APP_FRONT_CLIENT_SECRET);

$post = [
    'grant_type'   => 'authorization_code',
    'code'         => $_GET["code"],
    'redirect_uri' => APP_FRONT_URL
];

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL,"https://zoom.us/oauth/token");
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$code));

$response = json_decode(curl_exec($curl));
curl_close($curl);
$err  = curl_error($curl);

debugMessageAndObj("get user token", $response);

 if ($err) {
    printError("Erreur lors de la requete de email,  la connexion a échoué", $err);
}

if(!isset($response->access_token)) {printError("Erreur lors de requete de email, pas de token", $err); exit();}
$token = $response->access_token;

$curl3 = curl_init(); 
curl_setopt($curl3, CURLOPT_URL,        "https://api.zoom.us/v2/users/me"); 
curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true); 
curl_setopt($curl3, CURLOPT_HTTPHEADER, array( 
        "authorization: Bearer ".$token, 
        "content-type: application/json" 
 )); 
     curl_setopt($curl3, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

 $response3 = json_decode(curl_exec($curl3)); 
 $err3 = curl_error($curl3); 
 curl_close($curl3); 

 debugMessageAndObj("get user email", $response3);

 if ($err3) { 
     printError("Erreur lors de la requête de list de l'utilisateur, la connexion a échoué", $err3); 
 }

 if (!$response3->email)
 {
    print_r($response3);
 }
$zoomParticipantData = new stdClass();
$zoomParticipantData->email      = $response3->email;
$zoomParticipantData->first_name = $_SESSION["firstname"];
$zoomParticipantData->last_name  = $_SESSION["lastname"];

$res1 = $dbRegistered->query("SELECT * FROM registered WHERE email != '".$_SESSION["email"]."'AND zoom_email = '".$zoomParticipantData->email."'");

if($a = $res1->fetchArray()){
    header('Location: '.APP_URL.'from.php?error=zoom_account_already_used');
    debugMessage("redirect to from");
    exit();
}

$res = $dbRegistered->query("SELECT * FROM registered WHERE email = '".$_SESSION["email"]."'");

if($a = $res->fetchArray()){
    debugMessageAndObj("user from db", $a);
    
    if($a["zoom_email"] == $zoomParticipantData->email){
       header('Location: '.APP_URL.'from.php?success=zoom_account_unchanged');
       debugMessage("redirect to from");
       exit();
    } else { 
       //deny previous mail and let flow to the register part
       $response = deny($zoomParticipantData);
   
          if(isset($response->code, $response->message) && 
                            $response->code == 124 && 
                            $response->message == "Access token is expired."){
              
              if(refreshToken()){
                      $response = deny($zoomParticipantData);
                      if(!isset($response->code)){
                            $r = $dbRegistered->exec("DELETE FROM registered WHERE email = '".$_SESSION["email"]."'");
                            if(!$r) {printError("db delete error", $r); exit();}
                      } else {
                          printError("Erreur inconnue", $response);
                          exit();
                      }
              } else {
                  printError("Erreur lors de l'actualisation du token", $response);
                  exit();
              }
              
          } else if(isset($response->code)) {
              printError("Erreur inconnue", $response);
              exit();
   
          } else {
              $r = $dbRegistered->exec("DELETE FROM registered WHERE email = '".$_SESSION["email"]."'");
              if(!$r) {printError("db delete error", $r); exit();}
          }
     }
}


    $response = registerUser($zoomParticipantData);
        
    if(isset($response->join_url)){
        $r = $dbRegistered->exec("INSERT INTO registered(email, zoom_email, url) VALUES('".$_SESSION["email"]."', '".$zoomParticipantData->email."', '".$response->join_url."')");
        printAccessMessage($response);
    } else if(isset($response->code, $response->message) && 
            $response->code == 124 && 
            $response->message == "Access token is expired."){
        
        if(refreshToken()){
            $response = registerUser($zoomParticipantData);
        
                if(isset($response->join_url)){
                      $dbRegistered->exec("INSERT INTO registered(email, zoom_email, url) VALUES('".$_SESSION["email"]."', '".$zoomParticipantData->email."', '".$response->join_url."')");
                      printAccessMessage($response);
                } else {
                    printError("Erreur lors de l'actualisation du token", $response);
                }
            }
    } else {
        printError("Erreur inconnue", $response);
    }
?>